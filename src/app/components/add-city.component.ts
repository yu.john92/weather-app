import { Component, OnInit } from '@angular/core';
import { City } from '../models/city';
import { WeatherService} from '../services/weather.service';
import { Router } from '@angular/router';

@Component({
  selector: 'app-add-city',
  templateUrl: './add-city.component.html',
  styleUrls: ['./add-city.component.css']
})
export class AddCityComponent implements OnInit {
  location: City = { country:'', city:'', imageurl:''};
  result: string = "";

  constructor(private router: Router,
    private weatherSvc: WeatherService) {
  }

  ngOnInit() {
  }

  async onSubmit(){
    console.log("submit button clicked");
    console.log(this.location.city);
    console.log(this.location.country);
    console.log(this.location.imageurl);
    if(await this.weatherSvc.addCityIfValid(this.location)){
      console.log("Add Successful!");
      this.router.navigate(['/']);
    } else {
      console.log("Add Unsuccessful!");
      this.result="City not found!";
    };
  }
  clearForm(){
    console.log("clear button clicked");
    this.location.city='';
    this.location.country='';
    this.location.imageurl='';
    this.result='';
  }
  clearField(target: string){
    if(target==="city"){
      this.location.city='';
      this.result='';
    } else if (target==="country"){
      this.location.country='';
    } else if (target==="url"){
      this.location.imageurl='';
    }
  }

  loadPreviewImage() : string{
    return this.location.imageurl + '?' + (new Date()).getTime();
  }
}
