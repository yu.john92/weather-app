import { Component } from '@angular/core';
import { Router } from '@angular/router';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent {
  title = 'weather-app';
  addMode: boolean = false;
  constructor(private router: Router){
  
  }

  goHome(){
    this.router.navigate(['/']);
    this.addMode = false;
  }

  addCity(){
    this.router.navigate(['/add-city']);
    this.addMode = true;
  }
}
